export interface IGrid {
  label: string;
  value: number;
}

export interface ICoord {
  value: number;
  label?: string;
}

export interface ID3Data {
  x: number;
  y: number;
}

