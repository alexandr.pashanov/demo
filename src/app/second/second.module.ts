import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SecondRoutingModule } from './second-routing.module';
import {SecondComponent} from './second.component';
import {ForecastComponent} from '../components/forecast/forecast.component';
import {BaseWidgetComponent} from '../shared/base-widget/base-widget.component';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    SecondComponent,
    ForecastComponent,
    BaseWidgetComponent
  ],
  imports: [CommonModule, SecondRoutingModule, FormsModule],
  exports: [SecondComponent]
})
export class SecondModule {}
