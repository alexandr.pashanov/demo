import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'single-circle-left-graph',
  templateUrl: './single-circle-left-graph.component.html',
  styleUrls: ['./single-circle-left-graph.component.scss']
})
export class SingleCircleLeftGraphComponent implements OnInit {


  @Input() graphData = [];
  @Input() graphTitle = '';
  @Input() value = 0;
  @Input() valuePercentage = 0;
  @Input() PKM = 0;
  color = ['#98abc5', '#8a89a6', '#7b6888', '#6b486b', '#a05d56'];
  colorShadow = ['#eeeeee'];

  constructor() { }

  ngOnInit() {
  }

}
