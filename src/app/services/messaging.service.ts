import { BehaviorSubject } from "rxjs";

export class MessagingService {
  message = '';
  array = [];
  object = {};
  observableMessage = new BehaviorSubject<string>(this.message);
  observableData = new BehaviorSubject<any[]>(this.array);
  observableObject = new BehaviorSubject<{}>(this.object);
  constructor() { }

  changeMessage(message: string) {
    this.observableMessage.next(message);
  }

  changeArray(array: any[]) {
    this.observableData.next(array);
  }

  changeObject(object: {}) {
    this.observableObject.next(object);
  }
}
