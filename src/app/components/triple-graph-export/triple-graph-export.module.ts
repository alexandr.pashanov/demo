import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TripleGraphExportComponent } from './triple-graph-export.component';
import {ArcModule} from '../../shared/arc/arc.module';

@NgModule({
  declarations: [TripleGraphExportComponent],
  imports: [CommonModule, ArcModule],
  exports: [TripleGraphExportComponent, ArcModule]
})
export class TripleGraphExportModule {}
