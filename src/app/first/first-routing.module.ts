import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { FirstComponent } from './first.component';
import {TripleGraphExportComponent} from '../components/triple-graph-export/triple-graph-export.component';

export const routes: Routes = [
  {
    path: '',
    component: FirstComponent,
    children: [
      {
        path: 'triple',
        component: TripleGraphExportComponent
      },
      {
        path: 'semafor',
        component: TripleGraphExportComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FirstRoutingModule {}
