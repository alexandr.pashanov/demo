const app = require('express')();
const http = require('http').Server(app);
const port = process.env.PORT || 3000;

app.get('/api/mainscreen/smallGraph/:date/:id', (req, res) => {
  const id = req.params['id'];
  const date = req.params['date'];
  const json = require(`./json/smallGraph-${date}-${id}.json`);

  res.setHeader('Content-Type', 'application/json');
  res.send(json);
});

http.listen(port, () => {
  console.log('listening on *:' + port);
});
