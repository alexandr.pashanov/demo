import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConnectableObservable, Observable, throwError } from 'rxjs';
import { catchError, publishLast, retry } from 'rxjs/operators';
import { ISmallGraphsData} from '../interfaces/smallGraphData';

@Injectable({
  providedIn: 'root'
})
export class FetchService {
  constructor(private _http: HttpClient) {}

  public get(url: string): Observable<any> {
    const data = this._http.get(url).pipe(
      retry(3),
      catchError(this._handleError),
      publishLast()
    );
    (data as ConnectableObservable<any>).connect();
    return data;
  }

  public graphData(date: string, id): Observable<ISmallGraphsData[]> {
    return this.get(`/api/mainscreen/smallGraph/${date}/${id}`);
  }

  private _handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    return throwError('Something bad happened; please try again later.');
  }
}
