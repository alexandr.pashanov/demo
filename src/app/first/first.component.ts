import {AfterViewInit, Component, OnDestroy} from '@angular/core';
import { Subscription, timer} from 'rxjs';
import {FetchService} from '../services/fetch.service';
import {RefineryId} from '../enums/RefineryId';
import {ISmallGraphsData} from '../interfaces/smallGraphData';

@Component({
  selector: 'first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.scss']
})
export class FirstComponent implements AfterViewInit, OnDestroy {
  private date = '2019-07-15';
  private defaultGraphData: ISmallGraphsData[] = [{
      graphData: [{value: 0}],
      graphTitle: 'Title',
      value: 0,
      valuePercentage: 1,
      PKM: 0
    }];
  public graphData: ISmallGraphsData[];
  subscription: Subscription;

  constructor(private fetch: FetchService) { }

  ngAfterViewInit() {
    this.subscription = timer(0, 1000).subscribe(() => {
     this.fetch.graphData(this.date, RefineryId.Default).subscribe(result => {
        if (result) {
          this.graphData = result;
        } else  {
          this.graphData = this.defaultGraphData;
        }
      });
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
