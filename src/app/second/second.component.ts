import {Component, OnInit} from '@angular/core';
import {ICoord, IGrid} from '../interfaces/forecast.model';

@Component({
  selector: 'second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.scss']
})
export class SecondComponent implements OnInit {
  constructor() { }

  coords: ICoord[] = [
    {
      value: 30,
      label: 'АВТ-10'
    }, {
      value: 20,
      label: 'АТ-9'
    }, {
      value: 10,
      label: 'КПА'
    }, {
      value: 10,
      label: '43'
    }, {
      value: 30,
      label: 'АЛК-2'
    }, {
      value: 20,
      label: 'ГОБК'
    }, {
      value: 40,
      label: '35-11'
    }, {
      value: 50,
      label: 'ГФ'
    }, {
      value: 40,
      label: 'Девятый'
    }
  ];

  newCoords: ICoord[] = [
    {
      value: 10
    }, {
      value: 20
    }, {
      value: 40
    }, {
      value: 10
    }, {
      value: 20
    }, {
      value: 20
    }, {
      value: 50
    }, {
      value: 50
    }, {
      value: 40
    }
  ];

  grid: IGrid[] = [];

  ngOnInit() {
  }
}
