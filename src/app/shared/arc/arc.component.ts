import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  DoCheck,
  EventEmitter,
  Input,
  IterableDiffers,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { scaleOrdinal } from 'd3-scale';
import { arc, DefaultArcObject, pie } from 'd3-shape';
import { ArcData, ArcOption } from './arc.model';


interface D3Arc {
  data: D3DataEntries;
  endAngle: number;
  index: number;
  padAngle: number;
  startAngle: number;
  value: number;
}

interface D3DataEntries {
  key: string;
  value: number;
}


@Component({
  selector: 'arc',
  templateUrl: './arc.component.html',
  styleUrls: ['./arc.component.scss'],
  host: {
    '[class.arc]': 'true'
  },
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArcComponent implements DoCheck {
  private _scaleOrdinal: any;
  private _pi: number = Math.PI;
  private _optionsDefault: any = {
    back: {
      radius: 150,
      width: 10,
      padding: 0,
      paddingStart: 0,
      opacity: 1,
      margin: 0
    },
    unit: {
      radius: 150,
      width: 10,
      padding: 0,
      margin: 0,
      cornerRadius: 4,
      opacity: 1
    }
  };

  @Input() data: ArcData[] = [];
  @Input() color: string[] = ['#3a5997'];
  @Input() colorBack: string[] = ['#2c2f3c'];
  @Input() startAngle = 0;
  @Input() endAngle = 360;
  @Input()
  set options(value: ArcOption) {
    this._options = { ...this._options, ...value };
  }
  get options() {
    return this._options;
  }
  private _options = this._optionsDefault.unit;
  @Input()
  set optionsBack(value: ArcOption) {
    this._optionsBack = { ...this._optionsBack, ...value };

    if (this.options.width !== this._optionsBack.width) {
      this._optionsBack.paddingStart =
        (this.options.width - this._optionsBack.width) / 2;

      this._optionsBack.padding =
        this.options.width + this.options.padding - this._optionsBack.width;
    }
  }
  get optionsBack() {
    return this._optionsBack;
  }
  private _optionsBack = this._optionsDefault.back;

  protected _differ: any;
  public dataSource: any;
  public dataSourceBack: any;
  public getColorUnit: (d: D3Arc) => {};
  public getColorBack: (d: D3Arc) => {};
  @Output() hover: EventEmitter<any> = new EventEmitter();

  constructor(
    private _cdr: ChangeDetectorRef,
    protected _differs: IterableDiffers
  ) {
    this._scaleOrdinal = scaleOrdinal();
    this._differ = this._differs.find([]).create(null);
  }

  ngDoCheck() {
    const changes = this._differ.diff(this.data);

    if (changes) {
      this._onInit();
    }
  }

  private _onInit(): void {
    this.getColorUnit = (data: D3Arc) => this.getColor(this.color)(data.value);
    this.getColorBack = (data: D3Arc) =>
      this.getColor(this.colorBack)(data.value);

    this.dataSource = this._getData(this.data);
    this.dataSourceBack = this._getDataBack(this.data);

    this._cdr.markForCheck();
  }

  public getColor(colors: string[]) {
    return this._scaleOrdinal.range(colors);
  }

  public getArc(data: DefaultArcObject, index: number): string {
    return arc()
      .cornerRadius(this.options.cornerRadius)
      .innerRadius(this._getInnerRadius(index, this.options))
      .outerRadius(this._getOuterRadius(index, this.options))
      .startAngle(this._getStartAngle(data, index))
      .endAngle(this._getEndAngle(data, index))(data, index);
  }

  public getArcBack(data: DefaultArcObject, index: number): string {
    return arc()
      .innerRadius(this._getInnerRadiusBack(index, this.optionsBack))
      .outerRadius(this._getOuterRadiusBack(index, this.optionsBack))(
      data,
      index
    );
  }

  private _getInnerRadius(i: number, options: ArcOption) {
    const { radius, width, padding } = options;

    return radius - i * (width + padding) - width;
  }

  private _getInnerRadiusBack(i: number, options: ArcOption) {
    const { radius, width, padding, paddingStart } = options;

    return radius - i * (width + padding) - width - paddingStart;
  }

  private _getOuterRadius(i: number, options: ArcOption) {
    const { radius, width, padding } = options;

    return radius - i * (width + padding);
  }

  private _getOuterRadiusBack(i: number, options: ArcOption) {
    const { radius, width, padding, paddingStart } = options;

    return radius - i * (width + padding) - paddingStart;
  }

  private _getStartAngle(data?, i?: number) {
    return this.startAngle * (this._pi / 180);
  }

  private _getEndAngle(data?, i?: number) {
    const endAngle = this.endAngle * (this._pi / 180);

    if (data) {
      return (endAngle * data.data.value) / 100;
    }

    return endAngle;
  }

  private _getData(data: any): any {
    return pie()
      .value((d: any) => d.value)
      .startAngle(this._getStartAngle())
      .endAngle(this._getEndAngle())(data);
  }

  private _getDataBack(data: any): any {
    return data.map(item =>
      pie()
        .value((d: any) => d.value)
        .startAngle(this._getStartAngle())
        .endAngle(this._getEndAngle())([item])
    );
  }

  public moveToCenter(): string {
    const { radius, margin } = this.options;
    const halfEdge = radius + margin;

    return `translate(${halfEdge}, ${halfEdge})`;
  }

  public getViewBox(): string {
    const { radius, margin } = this.options;
    const edge = (radius + margin) * 2;

    return `0 0 ${edge} ${edge}`;
  }

  public onHover(event: Event, data: D3Arc, hovered: boolean): void {
    this.hover.emit({ source: data.data, hovered });
  }
}
