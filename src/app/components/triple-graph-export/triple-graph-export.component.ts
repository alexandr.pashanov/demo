import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'triple-graph-export',
  templateUrl: './triple-graph-export.component.html',
  styleUrls: ['./triple-graph-export.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TripleGraphExportComponent {
  @Input() title = 'Example graph';
  @Input() value = 560;
  @Input() singleIcon: string;
  @Input() arcsData = [{ value: 100 }, { value: 50 }, { value: 5 }];
  @Input() arcsColor = ['#3A5997', '#119DDC', '#FFFFFF'];
  @Input() arcsColorBack = ['#2C2F3C', '#2C2F3C', '#2C2F3C'];
  @Input() options = { padding: 20, margin: 5, width: 10 };
  @Input() optionsBack = { padding: 20, margin: 5, width: 10 };
  @Input() trendColor: string;
  @Input() showPKM = false;
  @Input() pkm: number;
  @Input() pkmColor = 'White';

  public getTrendIcon(): string {
    return this.trendColor === 'Green' ? 'trend-up' : 'trend-down';
  }

  public getPkmIcon(): string {
    return this.trendColor === 'Green' ? 'check_1' : 'decline_1';
  }
}
