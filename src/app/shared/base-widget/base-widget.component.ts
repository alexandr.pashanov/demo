import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'base-widget',
  templateUrl: './base-widget.component.html',
  styleUrls: ['./base-widget.component.scss']
})
export class BaseWidgetComponent implements OnInit {

  @Input() loading = false;
  @Input() title: string;
  @Input() icon: string;
  @Input() meta: string;


  constructor() {
  }

  ngOnInit() {
  }
}
