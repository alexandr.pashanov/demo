export interface ISmallGraphsData {
  graphData: IGraphValue[];
  graphTitle: string;
  value: number;
  valuePercentage: number;
  PKM: number;
}

interface IGraphValue {
    value: number;
}
