export interface ArcOption {
  radius?: number;
  width?: number;
  padding?: number;
  margin?: number;
  cornerRadius?: number;
  opacity?: number;
  paddingStart?: number;
}

export interface ArcData {
  value: number;
}
