import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InfoPageComponent} from './components/info-page/info-page.component';


const routes: Routes = [
  {
    path: '',
    component: InfoPageComponent
  },
  {
    path: 'first',
    loadChildren: () => import('./first/first.module').then(mod => mod.FirstModule)
  },
  {
    path: 'second',
    loadChildren: () => import('./second/second.module').then(mod => mod.SecondModule)
  },
  {
    path: '**',
    redirectTo: ''
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
