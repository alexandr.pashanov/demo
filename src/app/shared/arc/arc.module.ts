import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArcComponent } from './arc.component';

@NgModule({
  declarations: [ArcComponent],
  imports: [CommonModule],
  exports: [ArcComponent]
})
export class ArcModule {}
