import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirstRoutingModule } from './first-routing.module';
import {SingleCircleRightGraphComponent} from '../components/single-circle-right-graph/single-circle-right-graph.component';
import {SingleCircleLeftGraphComponent} from '../components/single-circle-left-graph/single-circle-left-graph.component';
import {FirstComponent} from './first.component';
import {ArcModule} from '../shared/arc/arc.module';
import {TripleGraphExportModule} from '../components/triple-graph-export/triple-graph-export.module';


@NgModule({
  declarations: [
    FirstComponent,
    SingleCircleLeftGraphComponent,
    SingleCircleRightGraphComponent
  ],
  imports: [CommonModule, FirstRoutingModule, ArcModule, TripleGraphExportModule],
  exports: [FirstComponent]
})
export class FirstModule {}
