import {Component, Input, OnInit} from '@angular/core';
import * as d3 from 'd3';
import {ICoord, ID3Data, IGrid} from '../../interfaces/forecast.model';




@Component({
  selector: 'forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit {
  constructor() { }
  @Input() coords: ICoord[];
  @Input() newCoords: ICoord[];
  @Input() width = 680;
  @Input() height = 200;

  graphBottomOffset = 70;
  graphLeftOffset = 30;
  graphTopOffset = 8;
  graphRightOffset = 30;
  graphHeight = this.height - this.graphBottomOffset - this.graphTopOffset;
  graphWidth = this.width - this.graphLeftOffset - this.graphRightOffset;

  D3Data: string;
  NewD3Data: string;
  FillD3Data: string;

  vGrid: IGrid[] = [];
  hGrid: IGrid[] = [{
    label: '',
    value: this.graphTopOffset
  }, {
    label: '',
    value: this.graphHeight + this.graphTopOffset
  }];

  getViewBox() {
    return `0 0 ${this.width} ${this.height}`;
  }

  getCoords(coords: ICoord[], reverse = false): ID3Data[] {
    const step = this.calculateStep();
    const D3Data: ID3Data[] = [];

    for (let i = reverse ? coords.length - 1 : 0; reverse ? i >= 0 : i < coords.length; reverse ? i-- : i++) {
      if (typeof i !== 'number') continue;
      D3Data.push({
        x: step * i + this.graphLeftOffset,
        y: this.calculateY(coords[i].value) + this.graphTopOffset
      });


      if (coords[i].label) {
        this.vGrid[i] = {
          value: step * i + this.graphLeftOffset,
          label: coords[i].label
        }
      }
    }

    return D3Data;
  }

  getCoordsD(coords: ICoord[], reverse = false) {
    const D3Data = this.getCoords(coords, reverse);
    const lineGenerator = d3.svg.line().interpolate('monotone').x(d => d.x).y(d => d.y);
    return lineGenerator(D3Data);
  }

  getFillD() {
    const D3Data: ID3Data[] = [
      ...this.getCoords(this.coords),
      ...this.getCoords(this.newCoords, true)
    ];

    const lineGenerator = d3.svg.line().interpolate('monotone').x(d => d.x).y(d => d.y);
    return lineGenerator(D3Data);
  }

  calculateStep() {
    const max = Math.max(this.coords.length, this.newCoords.length);
    return max > 1 ? (this.graphWidth) / (max - 1) : this.graphWidth;
  }

  calculateY(coord) {
    const max = this.getMax();
    const min = this.getMin();

    return this.graphHeight - (this.graphHeight * ((coord - min) / (max - min)));
  }

  getMax() {
    return Math.max(
      ...this.coords.map(c => c.value),
      ...this.newCoords.map(c => c.value)
    );
  }

  getMin() {
    return Math.min(
      ...this.coords.map(c => c.value),
      ...this.newCoords.map(c => c.value)
    );
  }

  ngOnInit() {
    this.graphHeight = this.height - this.graphBottomOffset - this.graphTopOffset;
    this.graphWidth = this.width - this.graphLeftOffset - this.graphRightOffset;

    this.D3Data = this.getCoordsD(this.coords);
    this.NewD3Data = this.getCoordsD(this.newCoords);
    this.FillD3Data = this.getFillD();
  }

}
